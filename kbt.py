from evdev import InputDevice, categorize, ecodes
import subprocess
import tempfile, os
import time
import concurrent.futures

import RPi.GPIO as g
from gpiozero import Button

GPIO_D0 = 6
GPIO_D1 = 13
GPIO_D2 = 26
GPIO_D3 = 5
#GPIO_LOWBAT = 4
BAT_LOW = Button(11)
#error===

g.setmode(g.BCM)
g.setwarnings(False)

g.setup(GPIO_D0, g.IN, pull_up_down=g.PUD_UP)
g.setup(GPIO_D1, g.IN, pull_up_down=g.PUD_UP)
g.setup(GPIO_D2, g.IN, pull_up_down=g.PUD_UP)
g.setup(GPIO_D3, g.IN, pull_up_down=g.PUD_UP)
#g.setup(GPIO_LOWBAT, g.IN)

keys = {2:'1', 3:'2', 4:'3', 5:'4', 6:'5', 7:'6', 8:'7',
        9:'8', 10:'9', 11:'0', 52:'.', 28:'ENTER' }

devices = ["event0", "event1"]    # List containing all keyboards
lastUtterance = time.monotonic()
batLowCounter = 0
#checkIdleTime = 60 * 15 # 15 Minutes
checkIdleTime = 20 # 20 secs at startup and then 15 Minutes once LOW_BAT = False

def getVolume():
    D0 = g.input(GPIO_D0)
    D1 = g.input(GPIO_D1)
    D2 = g.input(GPIO_D2)
    D3 = g.input(GPIO_D3)
    volLevel = D0+(D1*2)+(D2*4)+(D3*8)
    return volLevel

def ScanDevs():
    global old_devs
    devs = os.listdir("/dev/input/")
    print("Scan", devs)
    if old_devs != devs:
        old_devs = devs
        print("Dev update", devs)
        for dev in devs:
            if "event" in dev:
                devices.append(InputDevice("/dev/input/{}".format(dev)))
                print(devices)

vol = 50

try:
    (fd, wavfile) = tempfile.mkstemp(suffix='.wav', dir='/run/user/%d' % os.getuid())
except IOError:
    #logger.exception('Using fallback directory for TTS output')
    (fd, wavfile) = tempfile.mkstemp(suffix='.wav')
    os.close(fd)

def say(words):
    global wavfile, lastUtterance
    vol = (getVolume() + 1) * 6
    words = '<volume level="{}">{}'.format(vol, words)
    print(words)
    lastUtterance = time.monotonic()
    try:
        with open("/dev/null","w") as devnull:
            subprocess.call(["pico2wave", "-l", "en-GB", "-w", wavfile, words], stderr=devnull)
            subprocess.call(["aplay", wavfile], stderr=devnull)
    except Exception as e:
        print("Crikey!", e)
    finally:
        os.unlink(wavfile)

def checkIdle():
    global lastUtterance, checkIdleTime, batLowCounter
    print("In checkIdle")
    while True:
        #print("Low Battery =", BAT_LOW.is_pressed)
        if BAT_LOW.is_pressed == False: # False=Battery OK, True=Low Battery
            checkIdleTime = 60 * 15
            batLowCounter = 0
            msg = "15 minutes since last reading"
            if time.monotonic() - lastUtterance > checkIdleTime:
                say(msg)
        else:
            batLowCounter += 1
            checkIdleTime = 30
            msg = "Warning! Battery low!"
            say(msg)
            if batLowCounter > 2:
                msg = "System shutting down. - Please recharge the battery!"
                print("Martymic ended at {}".format(time.ctime(time.time())))
                say(msg)
                time.sleep(10)
                os.system("systemctl poweroff")
                #print("Halt sent!!!")
                
       
        time.sleep(30)

def readkeys(dev):
    print("In readkeys")
    while True:
        #devs = os.listdir(f"/dev/input/{dev}")
        #print(f"Dev={dev} devs={devs}")
        #if devs:
        try:
            device = InputDevice(f"/dev/input/{dev}")
        except Exception as e:
            pass
            #print("event not there:", e)
        else:
            try:
                measurement = ''
                for event in device.read_loop():
                    if event.type == ecodes.EV_KEY:
                        if event.value == 1:    # is the key down 1=down, 2=up, 3=hold?
                            try:
                                key = keys.get(event.code)
                            except: # traps unexpected non numeric values
                                pass
                            
                            if key == 'ENTER':
                                measurementFloat = float(measurement)
                                measurementRounded = round(measurementFloat, decimalPlaces)
                                print(measurementRounded)
                                say(measurementRounded)
                                measurement = ''
                                key = ''
                            else:
                                measurement += key

            except Exception as e:
                print("Failled, try again!", e)
                time.sleep(1)

def _main():
    global decimalPlaces
    x = 0
    print("Martymic stated at {}".format(time.ctime(time.time())))
    say("The Mouthy mike system is ready")

    digits = getVolume()
    decimalPlaces = getVolume() + 1
    say("Digits set to {}".format(decimalPlaces))

    with concurrent.futures.ThreadPoolExecutor() as executor:
        executor.map(readkeys, devices)
        executor.submit(checkIdle)
        #executor.submit(checkKnob)

    #print("Threads Ended")

_main()
